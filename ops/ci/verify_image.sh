set -e

if [ -z $ARTIFACT_BUILD_NUMBER ]; then
    echo "ERROR: Cannot verify the artifact without the ARTIFACT_BUILD_NUMBER set"
    exit 1
fi

echo "ARTIFACT_BUILD_NUMBER = ${ARTIFACT_BUILD_NUMBER}"

echo "Run image verification tests (eg .'test-kitchen') here."
