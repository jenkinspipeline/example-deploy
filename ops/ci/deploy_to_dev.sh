if [ -z $ARTIFACT_BUILD_NUMBER ]; then
    echo "ERROR: Cannot build artifact without the ARTIFACT_BUILD_NUMBER set"
    exit 1
fi

echo "Run deploy to DEVELOPMENT (eg. CloudFormation or Terraform) here."
