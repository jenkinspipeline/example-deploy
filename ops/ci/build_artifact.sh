if [ -z $PACKAGE_NAME ]; then
    echo "Cannot build artifact without the PACKAGE_NAME set"
    exit 1
fi

echo "PACKAGE_NAME            = ${PACKAGE_NAME}"

echo "Run package build (eg. Packer) here"
